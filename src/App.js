
import './App.css';
import Homeform from './components/Homeform';
import Nav from './components/Nav';
import Whybookwithus from './components/Whybookwithus';
import Awardsandrecognition from './components/Awardsandrecognition';
import Ourglobalpresence from './components/Ourglobalpresence';
import Numbers from './components/Numbers';
import Weareaboutbustravel from './components/Weareaboutbustravel';
import Explore from './components/Explore';
import Footer from './components/Footer';
import Dataget from './components/Dataget';
import Homepage from './components/Homepage';
import {BrowserRouter as Router,Route, Switch} from 'react-router-dom';
import Proceed from './components/Proceed';


function App() {
  return (
    <Router>
    <div className="App mx-0 px-0">
   
     
    
    <Switch>
    <Route exact path="/proceed" component={Proceed}/>
    <Route path="/" component={Homepage}/>
    </Switch>
    

    </div>
    </Router>
  );
}

export default App;

// <Whybookwithus/>
// <Awardsandrecognition/>
// <Ourglobalpresence/>
// <Numbers/>
// <Weareaboutbustravel/>
// <Explore/>
// <Footer/>






// <Dataget/>

// <Nav/>
// <Homeform/>
//     <Whybookwithus/>
//     <Awardsandrecognition/>
//     <Ourglobalpresence/>
//     <Numbers/>
//     <Weareaboutbustravel/>
//     <Explore/>
//     <Footer/>