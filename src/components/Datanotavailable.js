import React, { Component } from 'react';
import './datanotavailable.css';

export default class Datanotavailable extends Component {
  render() {
    return <div className='w-100 d-flex'>
    <div><img src="https://www.redbus.in/images/no_bus.png"/></div><div><h1>Oops! No buses found.</h1>No routes available</div>
    </div>;
  }
}
