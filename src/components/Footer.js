import React, { Component } from 'react'
import './footer.css';
import Logo from '../images/redBus-Logo.jpg'
import {AiFillFacebook} from 'react-icons/ai';
import{AiFillCopyrightCircle} from 'react-icons/ai'

export default class Footer extends Component {
    render() {
        return (
            <div className='f w-100 pb-5'>
            <div className='container'>
            <div className='row'>
            <div className='col-6 d-flex justify-content-between'>
            <div><div className='py-4'>About redBus</div>
            <div>About Us</div>
            <div>Contact Us</div>
            <div>Mobile Version</div>
            </div>
            <div><div className='py-4'>Info</div>
            <div>T & C</div>
            <div>Privacy Policy</div>
            <div>Cookie Policy</div>
            <div>FAQ</div>
            <div>Blog</div>
            </div>
            <div><div className='py-4'>Global Sites</div>
            <div>India</div>
            <div>Singapore</div>
            <div>Malaysia</div>
            <div>Indonesia</div>
            <div>Peru</div>
            <div>Columbia</div>
            </div>
            <div><div className='py-4'>Our Partners</div>
            <div>Goibibo</div>
            <div>Makemytrip</div>
            </div>

            </div>
            <div className='col-2 '>
            </div>
            <div className='col-4 '>
            <div className='py-4'><img src="https://www.redbus.com/images/home/sgp/r_logo.png"/></div>
            <div>
            redBus is the world's largest online bus ticket booking service trusted by over 25 million happy customers globally. redBus offers bus ticket booking through its website,iOS and Android mobile apps for all major routes.</div>
            <div><AiFillFacebook size={30}/></div>
            <div className='py-4'><AiFillCopyrightCircle className=''/>2021 ibibogroup All rights reserved</div>
            </div>
            </div>
                
            </div>
            </div>
        )
    }
}
