import React, { Component } from 'react'
import axios from 'axios';
import Whybookwithus from './Whybookwithus';
import Awardsandrecognition from './Awardsandrecognition';
import Ourglobalpresence from './Ourglobalpresence';
import Numbers from './Numbers';
import Weareaboutbustravel from './Weareaboutbustravel';
import Explore from './Explore';
import Footer from './Footer';
import Homeformtwo from './Homeformtwo';
import Nav from './Nav';
import Availablebus from './Availablebus';
import Filters from './Filters';

export default class Homepage extends Component {
    constructor(props) {
        super(props);
        this.state = {

            inputValue:null,
            status: 'notsubmitted',
            data:null,
            dataShow:null



        }
    }

    componentDidMount=()=>{
        axios({
            url:"https://api.robostack.ai/external/api/4b83ea48-026a-4bc7-a8b0-c84e9350a4f9/journeys/",
            method:"POST",
           headers:{
              
               "x-api-key":"7507bdce-65bc-411c-888b-8454988e171b",
               "x-api-secret":"a79d656d-9c39-4f67-accf-b78c2acbd4b4"



           }

        }).then((res)=>{
            console.log(res);
            this.setState({
                data:res.data
            })
        }).catch((err)=>{
            console.log(err);
        })

    }
    findData=(value)=>{
        // console.log(this.state.data);
        let {dataShow,data}={...this.state}
        let source=null;
        let destination=null;
    //   console.log(data.results);
   
      let v=data.results.filter((element)=>{
          return (element["startLocation_city_name"].toLowerCase()==value.from.toLowerCase() && element["stopLocation_city_name"].toLowerCase()==value.to.toLowerCase());
      })
      if(v.length!=0){
          this.setState({
              dataShow:v
          })
      }else{
        this.setState({
            dataShow:null
        })
              
    }
      

// console.log(v,'found data');
    }
    checkSubmit = (str,value) => {
        let{ inputValue,status} = { ...this.state };
        if (str == 'submitted') {
            console.log(str);
            status = 'submitted';
            inputValue=value;
             this.findData(value);
            this.setState({status })

        }

        
    }
    render() {
        console.log(this.state)
        return (
            <div>
            {
                (this.state.status==='notsubmitted')?<div><div className=''>
                <Nav />
               
                </div>
               
                </div>:
                <div>
                <Nav/>
              
                </div>
            }
            
            <Homeformtwo checkSubmit={this.checkSubmit} />
               {
               
                 (this.state.status==='notsubmitted')?
                 <div>
              
               
                
                <Whybookwithus />
                <Awardsandrecognition />
                <Ourglobalpresence />
                <Numbers />
                <Weareaboutbustravel />
                <Explore />
                <Footer />
                </div>
                :<div>
               
                <div className='container-fluid no-gutters'>
                <div className='row no-gutters'>
               <div className='col-2 no-gutters'>
                <Filters/>
                </div>
                <div className='col-10 no-gutters'>
                <Availablebus inputValue={this.state.inputValue} data={this.state.dataShow}/></div>
                </div>
                </div>
                </div>
               }
            </div>
        )
    }
}


// <Homeformtwo checkSubmit={this.checkSubmit} />