import React, { Component } from 'react'
import './weareaboutbustravel.css';

export default class Weareaboutbustravel extends Component {
    render() {
        return (
            <div className='container-fluid w'>
                <div className='row'>
                    <div className='col-12 text-center py-3'><h1>WE ARE ABOUT BUS TRAVEL</h1></div>
                </div>
                <div className='row  mx-5 pb-5 cu'>
                    <div className='col-5 h-25 cu'>redBus is the best platform in the world for booking bus tickets online. redBus is serving 6 countries around the world (India, Malaysia, Singapore, Indonesia, Peru, and Colombia), and would be launching bus ticket booking services in more countries. Trusted by over 18 million customers worldwide, redBus offers an easy, fast, and secure platform for booking cheap bus tickets. You only need to select your origin, destination, and date of departure, and you will find plenty of buses to travel by within a few seconds. You can book bus based on your preferred schedule, 
                    choose a pick-up and dropping point, preferred bus type, and book a bus ticket online with just a few clicks!</div>

                
               
                    <div className='col-3 h-25'>redBus operates on over 7000 routes and has on-boarded over 2,300 bus operators globally. In addition to bus tickets booking for popular tourist destinations, one can also book cheap bus tickets to the remote and less popular places in India, Malaysia, Singapore, Indonesia, Peru, and Colombia through redBus. redBus has sold over 180 million tickets globally through its website and mobile app, ensuring hassle-free and memorable experience for booking bus tickets online.</div>

              
               
                <div className='col-4 h-25'>Booking bus tickets online is simpler than ever with redBus! Considering the schedule, duration, bus operator, rating, fare, and the number of seats available, you can choose the most suitable seat and book a bus. Once you are done filling up the information, proceed to the payment page for completing the transaction online for bus bookings. Next, you need to download the e-ticket on your mobile phone and get ready to travel! Booking bus tickets with redBus keeps you away from long queues at bus terminals, bus operator’s offices and bus ticket booking counters.</div>
                </div>
            
            </div>
        )
    }
}
