import React, { Component } from 'react';
import './availablebus.css';
import Datanotavailable from './Datanotavailable';
import Boardingdropping from './Boardingdropping';
export default class Availablebus extends Component {
    constructor(props){
        super(props);
        this.state={
            seat:""
        }
    }
 
    render() {
        console.log(this.props.data);
        return (
            <div className='w-100 vh-100 no-gutters mb-5'>
            <div className='container-fluid  no-gutters'>
          
               {
                   (this.props.data!==null)?<div>
                   <div className='row dd py-5'>
                   <div className='col-3 d-flex justify-content-between'><div ><span className='font-bold'>{this.props.data.length} Buses</span><span className='font-grey'> found </span></div><div className='font-bold'>SORT BY:</div></div>
                   <div className='col-2 font-grey'>Departure</div>
                   <div className='col-1 font-grey'>Duration</div>
                   <div className='col-1 font-grey'>Arrival</div>
                   <div className='col-1 font-grey'>Ratings</div>
                   <div className='col-2 font-grey'>Fare</div>
                   <div className='col-2 font-grey'>Seats Available</div>
                   </div>
                 <div className='row py-3 border-bottom-dark'>
                 <div className='col font-size-20'>Showing <span ><span className='font-bold'>{this.props.data.length} </span>Buses from <span className='font-bold'>{this.props.data[0].startLocation_city_name} </span>to <span className='font-bold'>{this.props.data[0].stopLocation_city_name}</span></span></div>
                 </div>
                 { this.props.data.map((element)=>{
                     return( 
                         
                         <div className='row w-100 border  bus-div no-gutters bus pt-3 my-5 mx-0 px-0' key={element.id}>
                         <div className='container-fluid no-gutters px-0'> 
                         <div className='row '>
                          <div className='col-3 name '>{element.bus_name}</div>
                        <div className='col-2 '><span className='font-b '>{element.departure}</span><div className='font-grey'>{element.startLocation_city_name}</div></div>
                        <div className='col-1 font-grey'>{element.duration}</div>
                        <div className='col-1 '>{element.arrival}<div className='font-grey'>{element.stopLocation_city_name}</div></div>
                        <div className='col-1 green-bg d-flex align-items-center justify-content-center '><i className="far fa-star rating-img"></i>{element.ratings}</div>
                        <div className='col-2 '>INR<span className='font-b'>{element.fare}</span></div>
                        <div className='col-2 font-grey d-flex align-items-center'>
                        <div>{element.seatsAvailable} Seats available</div>
                       </div>
                        </div>





                        
                        <div className='row w-100 mx-0 px-0'>
                        <Boardingdropping id={element.id} buslist={this.props.data}/>
                        </div>
                      
                        </div>
                        </div>
                        )

                 })
                   }
                   
                   </div>:<div><Datanotavailable/></div>
               }
               </div>
            </div>
        )
    }
}
