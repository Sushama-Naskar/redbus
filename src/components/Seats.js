import React, { Component } from 'react';
import './seat.css'

export default class Seats extends Component {
    constructor(props){
        super(props);
    }
  render() {
    return <div className='w-100 d-flex flex-column justify-content-center align-items-center'>
    <div>The maximum number of seats that can be selected is 6</div>
    <div>Lower Deck</div>
    <div className='bus-deck w-100 d-flex flex-column justify-content-center align-items-center pb-3'>
  
    <div className='d-flex justify-content-between'>
    <div onClick={()=>{this.props.selectedSeat('1A')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('2A')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('3A')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('4A')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('5A')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('6A')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('7A')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    </div>
    <div className='d-flex justify-content-between'>
    <div onClick={()=>{this.props.selectedSeat('1B')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('2B')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('3B')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('4B')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('5B')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('6B')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('7B')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    </div>

   
    <div className='d-flex justify-content-between'>
    <div onClick={()=>{this.props.selectedSeat('1C')}} className='px-3 pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('2C')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('3C')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('4C')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('5C')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('6C')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('7C')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    </div>
    </div>




    <div>Upper Deck</div>
    <div className='bus-deck w-100 d-flex flex-column justify-content-center align-items-center pb-3'>
  
    <div className='d-flex justify-content-between'>
    <div onClick={()=>{this.props.selectedSeat('1D')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('2D')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('3D')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('4D')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('5D')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('6D')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('7D')}} className='px-3 py-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    </div>
    <div className='d-flex justify-content-between'>
    <div onClick={()=>{this.props.selectedSeat('1E')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('2E')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('3E')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('4E')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('5E')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('6E')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('7E')}} className='px-3'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    </div>

   
    <div className='d-flex justify-content-between'>
    <div onClick={()=>{this.props.selectedSeat('1F')}} className='px-3 pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('2F')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('3F')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('4F')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('5F')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('6F')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    <div onClick={()=>{this.props.selectedSeat('7F')}} className='px-3  pt-5'><div className=' outer-div d-flex align-items-center justify-content-end'><div className=' inner-div'></div></div></div>
    </div>
    </div>

    </div>;
  }
}
