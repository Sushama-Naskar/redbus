import React, { Component } from 'react'
import './whybookwithus.css';

export default class Whybookwithus extends Component {
    render() {
        return (
            <div className='b'>
            <div className='container h-55  pb-5'>
            <div className='row'>
            <div className='col-12 text-center py-3'><h2>WHY BOOK WITH US</h2></div>
            </div>
                <div className='row border row-background'>
                <div className='col-3 d-flex flex-column justify-content-center border text-center px-5 py-5'> 
                <div className=''><img src="https://s1.rdbuz.com/Images/webplatform/rbdotcom/multiple_bus_service.svg " className='img1'></img></div>
                <div className='h-75'><div className='py-5 h-25'>MULTIPLE BUS SERVICES</div>
                <div className='py-5'>Choose from various bus services, coach companies and your preferred seat.</div></div>
                

                </div>
                <div className='col-3 d-flex flex-column justify-content-center border text-center px-5 py-5'> 
                <div className=''><img src="https://s1.rdbuz.com/Images/webplatform/rbdotcom/secured%20payments.svg " className='img1'></img></div>
                <div className='h-75'>
                <div className='py-5 h-25'>SECURED PAYMENTS</div>
                <div className='py-5'>redBus has the highest security standards and keeps your information and purchases completely safe and secure</div>

                </div>
    
                </div>
                <div className='col-3 d-flex flex-column justify-content-center border text-center px-5 py-5'> 
                <div className=''><img src="https://s1.rdbuz.com/Images/webplatform/rbdotcom/global_coverage.svg " className='img1'></img></div>
                <div className='h-75'>
                <div className='py-5 h-25'>GLOBAL COVERAGE</div>
                <div className='py-5'>we constantly add bus supply for different countries. Get all your bus travel needs covered under one roof</div>

                </div>
     
                </div>
                <div className='col-3 d-flex flex-column justify-content-center border text-center px-5 py-5'> 
                <div><img src="https://s1.rdbuz.com/Images/webplatform/rbdotcom/customer_support.svg " className='img1'></img></div>
                <div className='h-75'>
                <div className='py-5 h-25'>SUPERIOR CUSTOMER SUPPORT</div>
                <div className='py-5'>Our customer support will ensure to service all your queries</div>

                </div>
     
                </div>
                </div>
            </div>
            </div>
        )
    }
}
