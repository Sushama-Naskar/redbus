import React, { Component } from 'react'
import './homeform.css';
import Office from '../images/office.png';
import Calendar from '../images/calendar.png';
import Exchange from '../images/exchange.png';
import Homepage from './Homepage';
import Nav from './Nav';
import Availablebus from './Availablebus';
import Searchlist from './Searchlist.js';
import Searchto from './Searchto.js';

export default class Homeform extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: {
                from: "",
                to: "",
                onwardDate: null,
                returnDate: null
            },
            errorValue: {
                fromError: null,
                toError: null,
                onwardDateError: null,
                returnDateError: null


            },
            formStatus: {
                status: 'notsubmitted'
            },
            searchValue: {
                fromSearch: "",
                toSearch: ""
            },
            buttonValue: 'SEARCH BUSES',
            selectfromPlace: false,
            selecttoPlace: false

        }
    }
    handleChange = (e) => {
        let city = ['Bangalore', "Kolkata", "Delhi", "Pune", "Mumbai", "Hyderabad", "Lucknow", "Bhopal", "Patna", "Jaipur", "Chennai", "Ranchi", "Agra", "Meerut", "Nagpur", "Kanpur", "Ahmedabad", "Surat", "Ludhiana"];
        let { inputValue, errorValue, searchValue, selectfromPlace, selecttoPlace } = { ...this.state };
        if (e.target.id == 'from') {
            selectfromPlace = "";
        }
        if (e.target.id == 'to') {
            selecttoPlace = false;
        }

        errorValue[e.target.id + 'Error'] = null;
        errorValue[e.target.id + 'Search'] = null;
        inputValue[e.target.id] = e.target.value;
        if (e.target.id == "from" || e.target.id == "to") {
            searchValue[e.target.id + 'Search'] = city.filter((element) => {

                if (e.target.value === "") {
                    return 0;
                }
                let lelement = element.toLowerCase();
                let val = e.target.value.toLowerCase()

                return (lelement.includes(val));
            });

        }
        this.setState({ inputValue, errorValue, searchValue, selectfromPlace, selecttoPlace });
    }
    fromValidate = (value) => {
        let city = ['Bangalore', "Kolkata", "Delhi", "Pune", "Mumbai", "Hyderabad", "Lucknow", "Bhopal", "Patna", "Jaipur", "Chennai", "Ranchi", "Agra", "Meerut", "Nagpur", "Kanpur", "Ahmedabad", "Surat", "Ludhiana"];
        let ch = city.find((element) => {
            let lelement = element.toLowerCase();
            let val = value.toLowerCase();
            return (val === lelement);
        })

        if (ch) {
            console.log(ch);
            return 0;
        } else {
            return 'Enter a valid name';
        }

    }
    toValidate = (value) => {
        let city = ['Bangalore', "Kolkata", "Delhi", "Pune", "Mumbai", "Hyderabad", "Lucknow", "Bhopal", "Patna", "Jaipur", "Chennai", "Ranchi", "Agra", "Meerut", "Nagpur", "Kanpur", "Ahmedabad", "Surat", "Ludhiana"];
        //   if(value===null || value===""){
        //       return "Enter a valid name";
        //   }
        let ch = city.find((element) => {
            let lelement = element.toLowerCase();
            let val = value.toLowerCase();
            return (val === lelement);
        })

        if (ch) {
            console.log(ch);
            return 0;
        } else {
            return 'Enter a valid name';
        }



    }
    onwardDateValidate = (value) => {

        if (value === null || value === "") {
            return "Choose a valid date";
        }
        return 0;

    }
    //   returnDateValidate=(value)=>{
    //     console.log(value);
    //       if(value===null){
    //           return "source";
    //       }
    //       return 0;

    //   }
    validateForm = (value) => {
        let { inputValue, errorValue ,searchValue} = { ...this.state };

        let emessage = 0;
        switch (value) {
            case "from": let from = this.fromValidate(inputValue['from']);
                if (from !== 0) {
                    emessage = 1;
                    errorValue.fromError = from;
                }else{
                    searchValue.fromSearch="";

                }
                break;
            case "to": let to = this.toValidate(inputValue['to']);
                if (to !== 0) {
                    emessage = 1;
                    errorValue.toError = to;
                }else{
                    searchValue.toSearch="";
                }
                break;
            case "onwardDate": let onwardDate = this.onwardDateValidate(inputValue['onwardDate']);
                if (onwardDate !== 0) {
                    emessage = 1;
                    errorValue.onwardDateError = onwardDate;
                }
                break;


            default: break;


        }
        this.setState({ errorValue })
        return emessage;
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let { formStatus } = { ...this.state };
        let checkValid = Object.keys(this.state.inputValue).map((element) => {
            return this.validateForm(element);
        })
        if (checkValid.includes(1)) {
            formStatus.status = 'notsubmitted';

        } else {
            formStatus.status = "submitted";
            if (this.state.buttonValue == 'SEARCH BUSES') {
                this.setState({
                    buttonValue: 'MODIFY'
                })
            } else if (this.state.buttonValue == 'MODIFY') {
                this.setState({
                    buttonValue: 'SEARCH'
                })

            } else {
                this.setState({
                    buttonValue: 'MODIFY'
                })

            }


            this.props.checkSubmit('submitted', this.state.inputValue);
        }


    }
    updatefromInput = (value) => {
        console.log(value);
        let { inputValue, selectfromPlace, errorValue } = { ...this.state };
        inputValue.from = value;
        selectfromPlace = true;
        errorValue.fromError = null;
        this.setState({ inputValue, selectfromPlace, errorValue })

    }

    updatetoInput = (value) => {
        console.log(value);
        let { inputValue, selecttoPlace, errorValue } = { ...this.state };
        inputValue.to = value;
        selecttoPlace = true;
        errorValue.toError = null;
        this.setState({ inputValue, selecttoPlace, errorValue })

    }
    exchangeValue = () => {
        let { inputValue } = { ...this.state };
        let temp = inputValue.to;
        inputValue.to = inputValue.from;
        inputValue.from = temp;
        this.setState({ inputValue });
    }
    render() {

        return (


            <div className='w-100 p'>
            <div className={`container-fluids d-flex  justify-content-center align-items-center mx-0 px-0 ${this.state.formStatus.status=='notsubmitted'?`  ${this.state.buttonValue=='SEARCH'?"not-sub-height":"c"}`:"submitted-form"} `}>
            <div className='row no-gutter' >
            <div className='col'>

            <form className={` form-row  d-flex flex-column justify-content-between ${this.state.formStatus.status=='submitted'?" sumitted-form-size":""} `} onSubmit={this.handleSubmit}>
            <div className='container white-bg mt-5'>
            <div className='row '>
            <div className={`col-3 justify-content-start i ${this.state.buttonValue=='SEARCH BUSES'?`${this.state.errorValue.fromError?"red-border":""}`:""}`}>
            {this.state.formStatus.status=='notsubmitted'? this.state.buttonValue=="SEARCH"?"":<div>
            <span className=''><img src={Office} className='input-image'/></span>
           

            </div>:null}
            <div>
            {this.state.buttonValue=="MODIFY"?"": <label htmlFor="from" >FROM</label>}
           
             <input value={this.state.inputValue.from} id="from" readOnly={`${this.state.buttonValue=='MODIFY'?"readonly":""}`} className={`${this.state.buttonValue=='MODIFY'?" s":`${this.state.buttonValue=='SEARCH'?"bob mr-3":""}`}`}  onChange={this.handleChange}/>
             
             <div className='error-message'>{this.state.buttonValue=='SEARCH'?<div className='error-message'>{this.state.errorValue.fromError}</div>:""}</div>
             </div>
             {this.state.buttonValue=='MODIFY'?<div className=' arrow-font d-flex align-items-center'>&rarr;</div>: <div className=' px-3 d-flex align-items-center justify-content-center darrow '><span className='border-grey-round ' onClick={()=>{this.exchangeValue()}}><img src={Exchange} className='input-image-darrow'/></span></div>}
             </div>
             
             
            
             <div className={`col-3 i ${this.state.buttonValue=='SEARCH BUSES'?`${this.state.errorValue.toError?"red-border":""}`:""}`}>
             {this.state.formStatus.status=='notsubmitted'?this.state.buttonValue=="SEARCH"?"":<div>
             <span className='px-3'><img src={Office} className='input-image'/></span>
            

            </div>:null}
             
             <div>
             {this.state.buttonValue=="MODIFY"?"":<label htmlFor="to">TO</label>}
             
              <input id="to" value={this.state.inputValue.to} readOnly={`${this.state.buttonValue=='MODIFY'?"readonly":""}`} className={`${this.state.buttonValue=='MODIFY'?" s":`${this.state.buttonValue=='SEARCH'?"bob mx-3":""}`}`} onChange={this.handleChange}/>
              <div className='error-message'>{this.state.buttonValue=='SEARCH'?<div>{this.state.errorValue.toError}</div>:""}</div>
              </div>
             
              </div>
             
              <div className={`col-4 i ${this.state.buttonValue=='SEARCH BUSES'?`${this.state.errorValue.onwardDateError?"red-border":""}`:""}`}>
              {this.state.formStatus.status=='notsubmitted'?this.state.buttonValue=="SEARCH"?"":<div>
              <span className='px-3'><img src={Calendar} className='input-image-calender'/></span>
              
 
             </div>:null}
            
              <div>
              
              {this.state.buttonValue=="MODIFY"?"": <label htmlFor="onwardDate">ONWARD DATE</label>}
              
               <input id="onwardDate" type="date" readOnly={`${this.state.buttonValue=='MODIFY'?"readonly":""}`} className={`${this.state.buttonValue=='MODIFY'?" s":`${this.state.buttonValue=='SEARCH'?"bob mx-3":""}`}`}  onChange={this.handleChange}/>
               <div className='error-message'>{this.state.buttonValue=='SEARCH'?<div>{this.state.errorValue.onwardDateError}</div>:""}</div>
               </div>
               </div>
               
            
              
               
             <div className=' col  color-white i d-flex justify-content-center'>
             <button className={`${this.state.buttonValue=="SEARCH BUSES"?"search-buses":`${this.state.buttonValue=='SEARCH'?"search-btn":"modify"}`}`} >
             {this.state.buttonValue}
            </button>
             </div>
             </div>
             </div>
             <div className='row my-3 '>
             <div className='col-3  d-flex justify-content-center'>{this.state.buttonValue=="MODIFY"?"":this.state.selectfromPlace==false?<div className={`l  ${this.state.searchValue.fromSearch.length>0?"place":"bg-trans"}`}><Searchlist data={this.state.searchValue.fromSearch} updatefromInput={this.updatefromInput}/></div>:null}</div>
             <div className='col-3   d-flex justify-content-center'>{this.state.buttonValue=="MODIFY"?"":this.state.selecttoPlace==false?<div className={`l ${this.state.searchValue.toSearch.length>0?"place":""}`}><Searchto data={this.state.searchValue.toSearch} updatetoInput={this.updatetoInput}/></div>:null}</div>
             <div className='col'></div>
          
             </div>
             </form>
             </div>
             </div>
             </div>
             
          
         
           </div>

        )
    }
}

// <form>
// <input placeholder='FROM'/>
// <input placeholder='TO'/>
// <input type="date"/>
// <input type="date"/>
// <button>SEARCH BUSES</button>
// </form>


// {this.state.formStatus.status=='notsubmitted'?<div>
// <div className='i'>
// <span className='px-3'><img src={Calendar} className='input-image'/></span>
// <label htmlFor="returnDate">RETURN DATE</label>
// <input id="returnDate" type="date" onChange={this.handleChange}/>
// </div>
// </div>:null}

// <div className='col '> <div >{this.state.selectfromPlace==false ?this.state.buttonValue=="MODIFY"?<div className='l'><Searchlist data={this.state.searchValue.fromSearch} updatefromInput={this.updatefromInput}/></div>:null:null}</div></div>
// <div className='col '> <div >{this.state.selecttoPlace==false ? this.state.buttonValue=="MODIFY"?<div className='l'><Searchto data={this.state.searchValue.toSearch} updatetoInput={this.updatetoInput}/></div>:null:null}</div></div>




// {this.state.formStatus.status=='notsubmitted'?`${this.state.buttonValue=='SEARCH'?"":"c"}`:
// <div className='i'>
// <span className='px-3'><img src={Calendar} className='input-image'/></span>
// <div>
// <label htmlFor="returnDate">RETURN DATE</label>
// <input id="returnDate" type="date" onChange={this.handleChange}/>
// </div>
// </div>
// }

