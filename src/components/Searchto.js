import React, { Component } from 'react';
import Office from '../images/office.png';
import './search.css';
export default class Searchlist extends Component {
  render() {
    return <div>
    {
        this.props.data?<div>
        {this.props.data.map((element)=>{
            return(
                <div key={element.length+Math.random()} onClick={()=>{this.props.updatetoInput(element)}} className='d-flex'>
                <div className='px-2'><img src={Office} className='img-office'/></div><div>{element}</div>
                </div>
            )
        })}
        </div>:null
    }
    </div>;
  }
}
