import React, { Component } from 'react';
import Proceed from './Proceed';
// import Modal from '@material-ui/core/Modal';
import ReactDOM from 'react-dom';
import { Modal } from 'react-responsive-modal';
import './boardingdropping.css'
import 'react-responsive-modal/styles.css';

import './userform.css';
import { Link } from 'react-router-dom';
import validator from 'validator';
import Calendar from '../images/calendar.png';


export default class Userform extends Component {


    constructor(props) {
        super(props);
        this.state = {
            inputValue: {
                name: null,
                male: null,
                female: null,
                age: null,
                email: null,
                phone: null,




            },
            errorValue: {
                nameError: null,
                ageError: null,
                emailError: null,
                phoneError: null
            },
            formStatus: {
                status: 'notsubmitted',
                message: null
            },
            open: false
        }
    }

    handleChange = (e) => {

        const { inputValue, errorValue } = { ...this.state };
        console.log(inputValue[e.target.id])
        errorValue[e.target.id + 'Error'] = null;

        if (inputValue[e.target.id] == 'male') {
            inputValue.male = true;
            inputValue.female = false;
        } else {
            inputValue.male = false;
            inputValue.female = true;

        }
        inputValue[e.target.id] = e.target.value;

        this.setState({ inputValue, errorValue });
    }

    nameValidate = (value) => {
        if (value === null) {
            return "Enter a valid name";
        }

        if (value.trim().length === 0) {
            return "Enter a valid name";
        } else if (value.length < 2 || value.length > 50) {
            return "Name should have between 2 to 50 characters";
        }

        return 0;
    }

    emailValidate = (value) => {
        if (value === null) {
            return "Enter a valid email";
        }
        if (validator.isEmail(value) === false) {
            return "Enter a valid email";
        }
        return 0;
    }

    ageValidate = (value) => {
        if (value === null) {
            return 'Enter a valid age';
        }

        if (value <= 0 || value > 130) {
            return 'Age should be between 1 to 130';
        }

        return 0;
    }

    phoneValidate = (value) => {
        if (value === null) {
            return "number is missing";
        }

        if (value.length !== 10) {
            return 'phone no must contain 10 digits'

        }
        if (value < 11111111) {
            return 'phone no is wrong'
        }

        return 0;
    }





    validateForm = (value) => {
        const { inputValue, errorValue } = { ...this.state };
        let emessage = 0;

        switch (value) {

            case "name": let name = this.nameValidate(inputValue['name']);
                if (name !== 0) {
                    emessage = 1;
                    errorValue.nameError = name;
                }
                break;

            case "email": let email = this.emailValidate(inputValue['email']);
                if (email !== 0) {
                    emessage = 1;
                    errorValue.emailError = email;
                }
                break;

            case "age":
                let age = this.ageValidate(inputValue['age']);
                if (age !== 0) {
                    emessage = 1;
                    errorValue.ageError = age;
                }
                break;

            case "phone": let phone = this.phoneValidate(inputValue['phone']);
                if (phone !== 0) {
                    emessage = 1;
                    errorValue.phoneError = phone;
                }
                break;





            default: break;
        }

        this.setState({ errorValue });
        return emessage;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log('button pressed');

        const { formStatus } = { ...this.state };

        let checkValid = Object.keys(this.state.inputValue).map((element) => {
            return this.validateForm(element);
        })

        if (checkValid.includes(1)) {
            formStatus.status = 'notsubmitted';
            formStatus.message = null;
        } else {
            formStatus.status = 'submitted';
            formStatus.message = 'Form submitted successfully';
        }

        this.setState({ formStatus });
    }


    handleClose = () => {
        let { open } = { ...this.state };
        open = false;

        this.setState({ open }
        );

    }
    handleOpen = (e) => {
        e.preventDefault();
        let { open, formStatus } = { ...this.state };
        open = true;
        formStatus.status = 'notsubmitted';
        this.setState({ open, formStatus }
        );
    }
    render() {
        console.log(this.props.list)
        return (<div>

            <button type="button" className="proceed-btn" onClick={this.handleOpen}>PROCEED TO BOOK</button>

            <Modal
                open={this.state.open}
                onClose={this.handleClose}



            >
                {this.state.formStatus.status === 'notsubmitted' ?
                    <div className='width-user-form '>
                        <form onSubmit={this.handleSubmit}>
                            <div>
                                <div className='pl-3 font-bold bottom-border d-flex- '>

                                    <div className='d-flex justify-content-center'><h1>Passenger Details</h1></div>
                                    <div className='d-flex pb-3'>
                                        <div className='user-icon-form bg-green d-flex justify-content-center align-items-center mx-2' ><i className="far fa-user user-form-icon pr-3 icon-3 "></i> </div>
                                        <div className=' d-flex align-items-center '>Passenger Information</div>
                                    </div>
                                </div>
                                <div>
                                    <div className=' bottom-border-dark py-3'>
                                        <div className='py-3 border-form px-3'>
                                            <div>Passenger 1 | Seat </div>
                                            <div className=' py-3'>
                                                <label htmlFor='name'>Name</label>

                                                <input id="name" placeholder='Name' className={`user-form-input ${this.state.errorValue.nameError ? "red-border" : ""}`} type="text" onChange={this.handleChange} />

                                            </div>
                                            <div>
                                                <div className='d-flex justify-content-between'>
                                                    <div>
                                                        <div>Gender</div>
                                                        <div className='w-25 d-flex align-items-center justify-content-between'>
                                                            <div className='d-flex align-items-center'><input type="radio" value="male" name="gender" className='user-radio ' id="male" onChange={this.handleChange} />
                                                                <label className='mx-2'>Male</label>
                                                            </div>
                                                            <div className='d-flex align-items-center'>
                                                                <input type="radio" value="female" name="gender" className='user-radio' checked id="female" onChange={this.handleChange} />
                                                                <label className='mx-2'>Female</label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div className='w-25'>
                                                        <label htmlFor='age'>Age</label>
                                                        <input placeholder='Age' id='age' className={`user-form-input ${this.state.errorValue.ageError ? "red-border" : ""}`} type="number" onChange={this.handleChange} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='bottom-border-dark py-2 font-bold d-flex'>

                                        <div className='user-icon-form bg-yellow d-flex justify-content-center align-items-center mx-2' ><i className="fal fa-envelope user-form-icon pr-3 icon-3 "></i> </div>

                                        <div >Contact Details</div>
                                    </div>
                                    <div className='bottom-border-dark pb-5'>

                                        <div className=''>
                                            <div>
                                                <div className='bg-light-yellow d-flex justify-content-center my-3'>Your ticket will be sent to these details</div></div>
                                            <div>
                                                <div><label htmlFor='email'>Email ID</label><input id="email" placeholder='Email ID' className={`user-form-input ${this.state.errorValue.emailError ? "red-border" : ""}`} type="text" onChange={this.handleChange} /></div>
                                                <div className='d-flex flex-column'><div>Phone</div><div className='d-flex '><div className=' phone-code '>+91</div><input id="phone" placeholder='Phone' className={`user-form-input ${this.state.errorValue.phoneError ? "red-border" : ""}`} type="number" onChange={this.handleChange} /></div>
                                                </div>
                                            </div>
                                            <div>

                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                    </div>
                                    <div >
                                        <div className='py-4 font-size-13'>By clicking on proceed, I agree that I have read and understood the TnCs and the Privacy Policy</div>
                                        <div className='d-flex width-35 justify-content-between font-bold'><div>Total Amount:  </div><div>INR{this.props.list.fare}</div></div>
                                        <div className='d-flex justify-content-end'><button className='book-btn'>PROCEED TO PAY</button></div>
                                    </div>
                                </div>

                            </div>

                        </form>
                    </div>
                    :
                    <div className='success-div'>
                        <div className='container success-shadow'>
                       
                            <div className='row py-3 px-3'>
                                <div className='col bottom-border py-5 red-font'>
                                    <h4>{this.props.list.bus_name}</h4>
                                </div>
                            </div>
                            <div className='row bottom-border py-5'>

                                <div className='col-2 d-flex align-items-center justify-content-center'><i className="far fa-calendar-alt submitted-icons"></i></div>
                                <div className='col-7'>
                                    <div className='grey-font'>Departure</div>
                                    <div className='font-bold'>{this.props.list.departure}</div>
                                </div>
                                <div className='col-3'>
                                    <div className='grey-font '>Seats</div>
                                    <div className='font-bold'>{this.props.seats.map((ele,index)=>{
                                        if(index==5){
                                            return ele;
                                        }
                                        return (ele+', ');
                                      })}</div>
                                </div>

                            </div>

                            <div className='row bottom-border py-5'>

                                <div className='col-2 d-flex align-items-center justify-content-center'><i class="fas fa-map-marker-alt submitted-icons"></i></div>
                                <div className='col-7'>
                                    <div className='grey-font'>Boarding Point</div>
                                    <div className='font-bold'>{this.props.list.startLocation_location}</div>
                                </div>
                                <div className='col-3'>
                                    <div className='grey-font '>Dropping Point</div>
                                    <div className='font-bold'>{this.props.list.stopLocation_location}</div>
                                </div>

                            </div>
          
                            <div className='row bottom-border py-5'>

                                <div className='col-2 d-flex align-items-center justify-content-center'><i class="far fa-calendar-alt submitted-icons"></i></div>
                                <div className='col-7'>
                                    <div className='grey-font'>Onward Fare</div>
                                    <div className='font-bold'>Total Payable</div>
                                </div>
                                <div className='col-3'>
                                    <div className='grey-font '>INR {this.props.list.fare}</div>
                                    <div className='font-bold'>{this.props.list.fare}</div>
                                </div>

                            </div>

                            <div className='row py-3 red-bg-light'>
                                <div className='col d-flex justify-content-start align-items-center'>
                                    <div><i className="far fa-user user-form-icon submitted-icons"></i></div>
                                    <div className='d-flex font-size-20 '>{this.state.inputValue.name}({this.state.inputValue.age},{this.state.inputValue.male == true ? <div>M</div> : <div>F</div>})</div></div>
                            </div>
                        </div>

                        <div className='row bottom-border py-3'>
                        <div className='col-12 '>
                      <button className='proceed-btn'> PROCEED TO PAYMENT</button></div>
                       
                        </div>

                    </div>}
            </Modal>






        </div>)
    }
}



// hello form submitted
// <div className='bottom-border red-font py-3'><h4>{this.props.list.bus_name}</h4></div>
// <div className='bottom-border d-flex flex-column justify-content-between align-items-center py-5 '>

//     <div className='d-flex justify-content-center align-items-start'>
//         <div><i class="far fa-calendar-alt submitted-icons"></i></div>
//         <div className=' d-flex flex-column justify-content-start align-items-start'>
//             <div className='font-grey'>Departure</div>
//             <div className='font-bold'> {this.props.list.departure}</div>
//         </div>
//     </div>
//     <div>
//         <div className='font-grey'>Seats</div>
//         <div className='font-bold'>32A</div>
//     </div>



// <div className='d-flex justify-content-center align-items-start'>
//         <div><i class="far fa-calendar-alt submitted-icons"></i></div>
//         <div className=' d-flex flex-column justify-content-start align-items-start'>
//             <div className='font-grey'>Departure</div>
//             <div className='font-bold'> {this.props.list.departure}</div>
//         </div>
//     </div>
//     <div>
//         <div className='font-grey'>Seats</div>
//         <div className='font-bold'>32A</div>
//     </div>

//     </div>




// <div>{this.props.list.startLocation_location}<div>{this.props.list.stopLocation_location}</div></div>
// <div>{this.state.inputValue.name}({this.state.inputValue.age}{this.state.inputValue.male == true ? <div>M</div> : <div>F</div>})</div>
// <div>{this.props.list.fare}</div>