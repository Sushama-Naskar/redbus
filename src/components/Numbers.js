import React, { Component } from 'react';
import './numbers.css'

export default class Numbers extends Component {
    render() {
        return (
            <div className='container'>
            <div className='row'>
            <div className='col-12 text-center pt-4'><h2>THE NUMBERS ARE GROWING!</h2></div>
            </div>
            <div className='row pb-5 justify-content-center'>
            <div className='col-4 w-55 d-flex flex-column justify-content-center  align-items-center text-center px-5 py-5 h-75'> 
            <div className=''>CUSTOMERS</div>
            <div className=' count'>36 M</div>
            <div className='w-30'>redBus is trusted by over 36 million happy customers globally</div>
            </div>
            <div className='col-4 w-55 d-flex flex-column justify-content-center  text-center px-5 py-5 h-75'> 
            <div className=' '>OPERATORS</div>
            <div className=' count'>3500</div>
            <div className='w-35'>network of over 3500 bus operators worldwide</div>
            </div>
            <div className='col-4 w-5 5d-flex flex-column justify-content-center  text-center px-5 py-5 h-75'> 
            <div className=' '>BUS TICKETS</div>
            <div className=' count'>220+ M</div>
            <div className='w-55'>Over 220 million trips booked using redBus</div>
            </div>
            </div>
                
            </div>
        )
    }
}
