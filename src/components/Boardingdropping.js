import React, { Component } from 'react';
import './boardingdropping.css';
import Proceed from './Proceed';
import Userform from './Userform';
import Seats from './Seats.js'

export default class Boardingdropping extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: "",
      received: false,
      btntxt: 'Book',
      showuserform: false,
      seatSelected: [],
      
    }
  }
  seatBooking = (id) => {
    //    console.log(id);
    //    console.log(this.props.buslist);
    let { list, received, btntxt,seatSelected } = { ...this.state };
    list = this.props.buslist.find((element) => {
      return element.id == id;
    })
    received = !this.state.received;
    seatSelected=[];
    if (this.state.received) {
      btntxt = 'Book';
    } else {
      btntxt = 'Cancel';
    }
    this.setState({ list, received, btntxt,seatSelected });
  }
  showUserform = () => {
    let { showuserform } = { ...this.state };
    showuserform = !this.state.showuserform;
    
    
    this.setState({
      showuserform
    })

  }
  selectedSeat = (id) => {
    let {seatSelected}={...this.state};
    if(seatSelected.length<6){
      if(seatSelected.includes(id)===false){
       
         console.log(typeof(seatSelected))
         seatSelected.push(id);
    

      }else{
        seatSelected=seatSelected.filter((element)=>{
          return Number(element)!=Number(id);
        })
        

      }
      this.setState({ seatSelected});

    }




    // if(seatSelected.includes(id)!==false){
    //   console.log('not selected');
    //   if(seatSelected.length<21){
    //     if(seatSelected.length<19){
    //       seatSelected=seatSelected+id+', ';
    //     }else{
    //       seatSelected=seatSelected+id;
    //     }
        
  
    //   }
     
    //   this.setState({ seatSelected})
    // }
   
  }
  render() {

    return (
      <div className='container-fluid py-0 mx-0'>

        {this.props.buslist ?

          <div className='row '>
            <div className='row w-100  d-flex justify-content-end align-items-end mx-0 px-0'>

              <div className='col-2 d-flex align-items-end justify-content-end px-0'>  {this.props.id ? <div><button onClick={() => { this.seatBooking(this.props.id) }} className='float-btn book-btn'>{this.state.btntxt}</button></div> : ""}</div>
            </div>

            {this.state.received ?
              <div className='row  w-100 blue-bg  mx-0 py-0'>
                <div className='col-6 d-flex flex-column justify-content-center align-items-center'>
                  <Seats selectedSeat={this.selectedSeat}  seatSelected={this.state.seatSelected}/>
                </div>
                {
                  this.state.seatSelected.length>0? <div className='col-6 py-5  d-flex justify-content-end'>
                    <div className='w-55 boarding-bg px-3'>
                      <div className='font-size-13 py-3'>
                        <div className='font-bold '>THIS BOOKING IS NON-REFUNDABLE</div>
                        <div>This booking falls under  the 100% cancellation charges window of the cancellation policy</div>
                      </div>
                      <div className='font-bold '><h4>Boarding & Dropping</h4></div>
                      <div className='bottom-border'>
                        <div className='d-flex justify-content-between py-3'><div className='font-grey font-size-25'>{this.state.list.startLocation_location}</div><div className='font-bold'>{this.state.list.departure}</div></div>
                        <div className='d-flex justify-content-between py-3'><div className='font-grey font-size-25'>{this.state.list.stopLocation_location}</div><div className='font-bold'>{this.state.list.arrival}</div></div>
                      </div>
                      <div className='bottom-border font-size-20 font-bold py-3 d-flex justify-content-between'><div>Seat NO.</div><div>{this.state.seatSelected.map((ele,index)=>{
                        if(index==5){
                            return ele;
                        }
                        return (ele+', ');
                      })}</div></div>
                      <div className='d-flex flex-column py-3'>
                        <div className='font-size-20 font-bold'>Fare Details</div>
                        <div>
                          <div className='py-3'>
                            <div className='d-flex justify-content-between '>

                              <div>Amount</div>
                              <div className='font-bold'>INR {this.state.list.fare}</div>
                            </div>
                            <div className='font-size-13'>Taxes will be calculated during payment</div>

                          </div>
                        </div>
                        <div className='red-font py-3 d-flex justify-content-end'><div>Show Fare Details</div></div>


                        <div><Userform list={this.state.list} seats={this.state.seatSelected}/></div>

                      </div>
                    </div>
                  </div> : <div className='col'>
                  <div>SEAT LEGEND</div>
                  <div></div>
                  <div>

                  </div>
                  </div>
                }
              </div>:null}
             </div> : ""}

      </div>)
  }
}

// <div>boarding:{this.props.data.startLocation_location}</div>
// <div>dropping:{this.props.data.stopLocation_location}</div>
// <div>bus:{this.props.data.bus_name}</div>
// <button>proceed to book</button>



// <button type="button" className="btn btn-primary"  onClick={()=>{this.showUserform()}}>

//           Proceed to book
//          </button>