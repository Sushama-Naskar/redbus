import React, { Component } from 'react';
import Colombia from '../images/colombia.png';
import './ourglobalpresence.css';
import India from '../images/india.png';
import Indonesia from '../images/indonesia.png';
import Malaysia from '../images/malaysia.png';
import Peru from '../images/peru.png';
import Singapore from '../images/singapore.png';

export default class Ourglobalpresence extends Component {
    render() {
        return (
            <div className='our-background'>
            <div className='container'>
            <div className='row'>
            <div className='col-12 text-center pt-5'><h1>OUR GLOBAL PRESENSE</h1></div>
            </div>
                <div className='row pt-4'>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5  '> 
                <div className=' '><img src={Colombia} className='img-flag'></img></div>
                <div className=''>COLOMBIA</div>
                </div>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5 '> 
                <div className=' '><img src={India} className='img-flag'></img></div>
                <div className=''>INDIA</div>
                </div>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5 '> 
                <div className=' '><img src={Indonesia} className='img-flag'></img></div>
                <div className=''>INDONESIA</div>
                </div>
                </div>
               
                <div className='row '>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5 py-5 '> 
                <div className=' '><img src={Malaysia} className='img-flag'></img></div>
                <div className=''>MALAYSIA</div>
                </div>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5 py-5 '> 
                <div className=''><img src={Peru} className='img-flag'></img></div>
                <div className=''>PERU</div>
                </div>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5 py-5 '> 
                <div className=''><img src={Singapore} className='img-flag'></img></div>
                <div className=''>SINGAPORE</div>
                </div>
                </div>
            </div>
            </div>
        )
    }
}
