import React, { Component } from 'react'
import './homeform.css';
import Office from '../images/office.png';
import Calendar from '../images/calendar.png';
import Exchange from '../images/exchange.png';
import Homepage from './Homepage';
import Nav from './Nav';
import Availablebus from './Availablebus';

export default class Homeform extends Component {
    constructor(props){
        super(props);
        this.state={
            inputValue:{
                from:null,
                to:null,
                onwardDate:null,
                returnDate:null
            },
            formStatus:{
                status:'notsubmitted'
            }
           
            
            
        }
    }
    handleChange=(e)=>{
        const {inputValue}={...this.state};
        inputValue[e.target.id]=e.target.value;
        this.setState({inputValue});
    }
    fromValidate=(value)=>{
      console.log(value);
        if(value===null){
            return "source";
        }
        return 0;

    }
    toValidate=(value)=>{
        console.log(value);
          if(value===null){
              return "destination";
          }
          return 0;
  
      }
      onwardDateValidate=(value)=>{
        console.log(value);
          if(value===null){
              return "source";
          }
          return 0;
  
      }
    //   returnDateValidate=(value)=>{
    //     console.log(value);
    //       if(value===null){
    //           return "source";
    //       }
    //       return 0;
  
    //   }
    validateForm=(value)=>{
        const{inputValue}={...this.state};
        let emessage=0;
        switch(value){
            case "from": let from=this.fromValidate(inputValue['from']);
            if(from!==0){
                emessage=1;
            }
            break;
            case "to": let to=this.toValidate(inputValue['to']);
            if(to!==0){
                emessage=1;
            }
            break;
            case "onwardDate": let onwardDate=this.onwardDateValidate(inputValue['onwardDate']);
            if(onwardDate!==0){
                emessage=1;
            }
            break;
           
           
            default:break;

            
        }
        return emessage;
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        const {formStatus}={...this.state};
        let checkValid=Object.keys(this.state.inputValue).map((element)=>{
            return this.validateForm(element);
        })
      if(checkValid.includes(1)){
          formStatus.status='notsubmitted';
      }else{
          formStatus.status="submitted";
      }
      this.setState({formStatus});

    }
    render() {
        return (
            <div>
            {(this.state.formStatus.status==='notsubmitted')?
            <div>
            <Nav/>
            <div className='w-100'>
            <div className='container-fluids d-flex flex-cloumn justify-content-center align-items-center  mx-0 px-0  c '>
            <div className='w-75'>
            <form className='d-flex flex-sm-column flex-md-row justify-content-center' onSubmit={this.handleSubmit}>
            <div className='i'>
            <span className='px-3'><img src={Office} className='input-image'/></span>
            <div >
            <label htmlFor="from" >FROM</label>
             <input placeholder='h' id="from" onChange={this.handleChange} autoComplete='off'/>
             </div>
             </div>
             <span className='px-3 d-flex align-items-center darrow'><span className='border-grey-round'><img src={Exchange} className='input-image-darrow'/></span></span>
             <div className='i'>
             <span className='px-3'><img src={Office} className='input-image'/></span>
             <div>
             <label htmlFor="to">TO</label>
              <input id="to" onChange={this.handleChange}/>
              </div>
              </div>
              <div className='i'>
              <span className='px-3'><img src={Calendar} className='input-image'/></span>
              <div>
              <label htmlFor="onwardDate">ONWARD DATE</label>
               <input id="onwardDate" type="date" onChange={this.handleChange}/>
               </div>
               </div>
               <div className='i'>
               <span className='px-3'><img src={Calendar} className='input-image'/></span>
               <div>
               <label htmlFor="returnDate">RETURN DATE</label>
               <input id="returnDate" type="date" onChange={this.handleChange}/>
                </div>
                </div>
             <div className='i'>
             <button className='search-buses'>SEARCH BUSES</button>
             </div>
             </form>
             </div>
             </div>
            </div>
            <Homepage/></div>:
            <div><Availablebus inputValue={this.state.inputValue}/></div>
            }
            </div>
            
        )
    }
}

// <form>
// <input placeholder='FROM'/>
// <input placeholder='TO'/>
// <input type="date"/>
// <input type="date"/>
// <button>SEARCH BUSES</button>
// </form>