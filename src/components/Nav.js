import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './nav.css';
import Logo from '../images/redBus-Logo.jpg';
import Downarrow from '../images/down-arrow.png';
import {Link} from 'react-router-dom';

export default class Nav extends Component {
    render() {
        return (
          <div className='navbar '>
          <div className="container ">
    <a className="navbar-brand" href="">
   
    <img src="https://www.redbus.com/i/59538b35953097248522a65b4b79650e.png" className='red-bus-logo'/>
    </a>
    
         <div className='d-flex  align-items-center'>
         <div className='nav-item  '><a className="nav-link" href="">Help</a></div>
          <div className="dropdown ">
          <button className="sing-up-icon d-flex align-items-center justify-content-center " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            English
            <div className='icon-down px-3'><i className="fal fa-angle-down down-arrow fa-2x"></i></div>
          </button>
         
          <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a className="dropdown-item" href="">English</a></li>
            <li><a className="dropdown-item" href="">Espanol</a></li>
            <li><a className="dropdown-item" href="">български</a></li>
          </ul>
        </div>
        <div className="dropdown">
        <button className="sing-up-icon  d-flex align-items-center justify-content-center " type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
          INR
          <div className='icon-down px-3'><i className="fal fa-angle-down down-arrow fa-2x"></i></div>
        </button>
        
        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton2">
          <li><a className="dropdown-item" href="">Action</a></li>
          <li><a className="dropdown-item" href="">Another action</a></li>
          <li><a className="dropdown-item" href="">Something else here</a></li>
        </ul>
      </div>
      <div className="dropdown ">
      <button className="sing-up-icon  d-flex align-items-center justify-content-center  " type="button" id="dropdownMenuButton3" data-bs-toggle="dropdown" aria-expanded="false">
        Manage Booking
        <div className='icon-down px-3'><i className="fal fa-angle-down down-arrow fa-2x"></i></div>
      </button>
      
      <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton3">
        <li><a className="dropdown-item" href="">Action</a></li>
        <li><a className="dropdown-item" href="">Another action</a></li>
        <li><a className="dropdown-item" href="">Something else here</a></li>
      </ul>
    </div>
    <div className="dropdown ">
    <button className="sing-up-icon d-flex align-items-center justify-content-center" type="button" id="dropdownMenuButton4" data-bs-toggle="dropdown" aria-expanded="false">
    <i className="far fa-user-circle fa-2x"></i>
      <div className='icon-down px-3'><i className="fal fa-angle-down down-arrow fa-2x"></i></div>
    </button>
    
    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton4">
      
      <li><button type="button" className=" sign-nav-btn" data-bs-toggle="modal" data-bs-target="#exampleModal">
      Sign In/ Sign Up
    </button></li>
     
    </ul>
  </div>
  </div>
        </div>
        <div className="modal  fade" id="exampleModal" tabIndex="1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
            
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body  d-flex">
           
              <div className="w-50"><img src="https://s3.rdbuz.com/Images/webplatform/contextualLogin/desktop-payment-offers.svg" className='sign-img'/></div>
              <div className="w-50  right-sign px-4">
              <div className='pt-3'><img src="https://s3.rdbuz.com/Images/logo_r.png"/></div>
              <div className='py-3'><h3 className='mx-3 red-font'>Sign in to avail exciting discounts and cashbacks!!</h3></div>
              <div className=' d-flex align-items-center  px-3'><div className='font-size-10 px-3'>+91</div> <input type="number" placeholder='Enter your mobile number'/></div>
              <div className='pt-5 '><button className='sign-btn red-bg down-arrow'>GENERATE OTP (One Time Password)</button></div>
              <div className='text-center py-3'>OR, Connect using social accounts</div>
              <div className='py-3'><div><img/></div><div>Sign in with Google</div></div>
              <div className='py-3 d-flex justify-content-center '><div className='w-25'><button></button><img/></div><div>Facebook</div></div>
              <div className='text-center font-grey'>By signing up, you agree to </div>
              <div className='text-center'><span className='font-grey'>our </span><span className='font-blue'>Terms & conditions</span> <span className='font-grey'>and </span><span className='font-blue'>Privacy Policy</span> </div>
              </div>
              
            </div>
           
          </div>
        </div>
      </div></div>
      
        )
    }
}
