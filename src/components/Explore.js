import React, { Component } from 'react';
import './explore.css'

export default class Explore extends Component {
    render() {
        return (
            <div className='explore-backgroud'>
            <div className='continer  px-5 border'>
            <div >
            <h1>Explore top routes in Europe</h1>
            </div>
                <div className='d-flex justify-content-between align-items-center'>
                <div className='d-flex flex-column  align-items-start'>
                <div className=''>Spain
                </div>
                <div>
                <ul className='mx-0'>
                <li className='mx-0'>Alicante to Madrid Bus</li>
                <li>Barcelona to Valencia Bus</li>
                <li>Madrid to Barcelona Bus</li>
                <li>Madrid to Segovia Bus</li>
                <li>Madrid to Seville Bus</li>
                <li>Seville to Madrid Bus</li>
                <li>Valencia to Barcelona Bus</li>

                </ul>
                </div>
                </div>
                <div >
                <div>Germany
                </div>
                <ul>
                <li>Berlin To Krakow Bus</li>
                <li>Berlin To Prague Bus</li>
                <li>Berlin To Szczecin Bus</li>
                <li>Berlin To Warsaw Bus</li>
                <li>Dresden To Prague Bus</li>
                <li>Munich To Paris Bus</li>
                <li>Stuttgart To Paris Bus</li>
                </ul>
                </div>
                <div >
                <div>Poland
                </div>
                <ul>
                <li>Warsaw To Amsterdam Bus</li>
                <li>Warsaw To Berlin Bus</li>
                <li>Warsaw To Brussels Bus</li>
                <li>Warsaw To Kiev Bus</li>
                <li>Warsaw To Lviv Bus</li>
                <li>Warsaw To Munich Bus</li>
                <li>Warsaw To Prague Bus</li>
                </ul>
                </div>
                <div >
                <div>UK
                </div>
                <ul>
                <li>Bath To Bristol Bus</li>
                <li>Bristol To Bath Bus</li>
                <li>Leeds To Bradford Bus</li>
                <li>London To Luton Bus</li>
                <li>London To Oxford Bus</li>
                <li>Luton To London Bus</li>
                <li>Shipley To Leeds Bus</li>
                </ul></div>
                <div >
                <div>Bulgaria
                </div>
                <ul>
                <li>Burgas To Istanbul Bus</li>
                <li>Sofia To Athens Bus</li>
                <li>Sofia To Istanbul Bus</li>
                <li>Sofia To Plovdiv Bus</li>
                <li>Sofia To Sunny Beach Bus</li>
                <li>Sofia To Varna Bus</li>
                <li>Sofia To Vienna Bus</li>
                </ul></div>
                <div >
                <div>Croatia
                </div>
                <ul>
                <li>Pula To Rovinj Bus</li>
                <li>Split To Zadar Bus</li>
                <li>Split To Zagreb Bus</li>
                <li>Turin To Rome Bus</li>
                <li>Vela Luka To Split Bus</li>
                <li>Zadar To Zagreb Bus</li>
                <li>Zagreb To Ljubljana Bus</li>
                </ul></div>

                </div>
            </div>
            </div>
        )
    }
}
