import React, { Component } from 'react';
import './award.css';

export default class Awardsandrecognition extends Component {
    render() {
        return (
            
            <div className='container-fluid'>
            <div className='row'>
            <div className='col-12 text-center pt-4'><h2>AWARDS & RECOGNITION</h2></div>
            </div>
                <div className='row '>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5 py-5 '> 
                <div className=' py-5 h-75'><img src="https://s2.rdbuz.com/web/images/home/awards/Business_Standard1.png" className='img2'></img></div>
                <div className='py-5 '>Most Innovative Company</div>
                </div>
                <div className='col-4 d-flex flex-column justify-content-center  text-center px-5 py-5 '> 
                <div className='py-5 h-75'><img src="https://s1.rdbuz.com/web/images/home/awards/Brand_Trust_Report.png" className='img2'></img></div>
                <div className='py-5'>Most Innovative Company</div>
                </div>
                <div className='col-4 d-flex flex-column justify-content-center text-center px-5 py-5 '> 
                <div className=' py-5 h-75'><img src="https://s3.rdbuz.com/web/images/home/awards/Eye_for_Travel1.png" className='img2'></img></div>
                <div className='py-5 '>Most Innovative Company</div>
                </div>
                </div>
                
            </div>
        )
    }
}
